import MyPokemon from "./MyPokemon";
import Pokedex from "./Pokedex";
import PokemonDetail from "./PokemonDetail";

export { MyPokemon, Pokedex, PokemonDetail };
