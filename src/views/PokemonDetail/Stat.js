import { css } from "@emotion/react";
import React from "react";

const Stat = ({ stat }) => {
  const styles = {
    stat: css`
      display: flex;
      width: 50%;
      justify-content: flex-end;
      align-items: center;
      column-gap: 5px;
    `,
    statInfo: css`
      background-color: green;
      height: 10px;
      width: ${stat}%;
      border-radius: 10px;
    `,
  };
  return (
    <span css={styles.stat}>
      <b>{stat}</b>
      <span css={styles.statInfo}></span>
    </span>
  );
};

export default Stat;
