import { css } from "@emotion/react";
import React from "react";
import { capitalizeFirstLetter } from "../../utils";
import Stat from "./Stat";

const styles = {
  stats: css`
    display: flex;
    flex-direction: column;
    row-gap: 5px;
    width: 80%;
    margin: 0 2rem;
  `,
  heading: css`
    margin: 0;
  `,
  stat: css`
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: small;
  `,
};
const Status = ({ status }) => {
  return (
    <div css={styles.stats}>
      <h5 css={styles.heading}>Status</h5>
      {status !== undefined && Array.isArray(status)
        ? status.map((stat) => {
            return (
              <div css={styles.stat}>
                {capitalizeFirstLetter(stat.stat.name)}{" "}
                <Stat stat={stat.base_stat} />
              </div>
            );
          })
        : null}
    </div>
  );
};

export default Status;
