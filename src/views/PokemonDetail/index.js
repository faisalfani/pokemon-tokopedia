import { css, keyframes } from "@emotion/react";
import React, { useContext } from "react";
import { useParams } from "react-router-dom";
import { BreadCrumb, PokemonImage, PokemonTypes } from "../../components";
import { PokemonContext } from "../../context/PokemonContext";
import { useGetType } from "../../hooks";
import { getBackground } from "../../utils";
import Status from "./Status";

const PokemonDetail = () => {
  const { name } = useParams();
  const { setPokemonName, pokemon, pokemons } = useContext(PokemonContext);
  console.log(pokemon);
  setPokemonName(name);
  const types = useGetType(name);
  const image =
    pokemons !== undefined &&
    pokemons.length > 0 &&
    pokemons.filter((data) => data.name == name)[0]?.image;
  console.log("image", image);

  const shake = keyframes`
  0% { transform: translate(1px, 1px) rotate(0deg); }
  10% { transform: translate(-1px, -2px) rotate(-1deg); }
  20% { transform: translate(-3px, 0px) rotate(1deg); }
  30% { transform: translate(3px, 2px) rotate(0deg); }
  40% { transform: translate(1px, -1px) rotate(1deg); }
  50% { transform: translate(-1px, 2px) rotate(-1deg); }
  60% { transform: translate(-3px, 1px) rotate(0deg); }
  70% { transform: translate(3px, 1px) rotate(-1deg); }
  80% { transform: translate(-1px, -1px) rotate(1deg); }
  90% { transform: translate(1px, 2px) rotate(0deg); }
  100% { transform: translate(1px, -2px) rotate(-1deg); }
`;

  const styles = {
    container: css`
      height: 85vh;
      background-color: ${getBackground(types)};
      margin: 0 2rem;
      border: solid black 3px;
      border-radius: 10px;
      border-bottom: solid black 6px;
      display: flex;
      flex-direction: column;
      align-items: center;
      row-gap: 0.5rem;
    `,
    pokemonImage: css`
      height: 30%;
    `,
    descriptionContainer: css`
      background-color: white;
      width: 100%;
      max-height: 80%;
      flex: 1;
      overflow-y: auto;
      border-radius: 10px;
      display: flex;
      flex-direction: column;
      row-gap: 0.5rem;
      align-items: center;
    `,
    pokeInfo: css`
      display: flex;
      column-gap: 10px;
      padding: 1rem;
    `,
    button: css`
      font-size: medium;
      font-weight: bold;
      border-radius: 10px;
      border: solid black 3px;
      border-bottom: solid black 6px;
      padding: 8px 12px;
      cursor: pointer;
      animation: ${shake} 0.5s;
    `,
  };

  return (
    <>
      <div css={styles.container}>
        <BreadCrumb pageTitle={name} color="white" />
        <PokemonImage src={image} styles={styles.pokemonImage} />
        <PokemonTypes types={types} row />
        <div css={styles.descriptionContainer}>
          <div css={styles.pokeInfo}>
            <span>
              <b>weight</b> {parseInt(pokemon?.weight) / 10} kg
            </span>
            <span>
              <b>height</b> {parseInt(pokemon?.height) / 10} m
            </span>
          </div>
          <Status status={pokemon?.stats} />
          <div css={styles.button}>Catch the Pokemon</div>
        </div>
      </div>
    </>
  );
};

export default PokemonDetail;
