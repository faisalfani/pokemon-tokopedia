import React from "react";
import { BreadCrumb } from "../../components";

const MyPokemon = () => {
  return (
    <>
      <BreadCrumb pageTitle="MyPokemon" />
    </>
  );
};

export default MyPokemon;
