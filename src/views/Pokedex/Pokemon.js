import { css } from "@emotion/react";
import React from "react";
import { PokemonImage, PokemonTypes } from "../../components";
import { useGetType } from "../../hooks";
import { capitalizeFirstLetter, getBackground, mq } from "../../utils";
import memphis1 from "./../../assets/img/memphis1.png";

const Pokemon = ({ pokemon }) => {
  const types = useGetType(pokemon.name);
  const styles = {
    pokemonBox: css`
      background-color: ${getBackground(types)};
      padding: 0.5rem;
      border-radius: 10px;
      border: solid black 3px;
      border-bottom: solid black 6px;
      height: 20vh;
      position: relative;
      cursor: pointer;
      background-image: url(${memphis1});
      background-repeat: no-repeat;
      background-position: bottom -50px right -20%;
    `,
    pokemonName: css`
      color: white;
      font-weight: bold;
      font-size: larger;
    `,

    pokemonImage: css`
      height: 65%;
      max-width: 70%;
      width: auto;
      position: absolute;
      bottom: 0;
      right: 0;
    `,
  };
  return (
    <div key={pokemon.id} css={styles.pokemonBox}>
      <div css={styles.pokemonName}>{capitalizeFirstLetter(pokemon.name)}</div>
      <PokemonTypes types={types} />
      <PokemonImage src={pokemon.image} styles={styles.pokemonImage} />
    </div>
  );
};

export default Pokemon;
