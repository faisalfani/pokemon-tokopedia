import { css } from "@emotion/react";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { PokemonContext } from "../../context/PokemonContext";
import { capitalizeFirstLetter, mq } from "../../utils";
import Pokemon from "./Pokemon";

const styles = css`
  display: grid;
  grid-column-gap: 1rem;
  grid-row-gap: 1rem;
  grid-template-columns: 1fr 1fr;
  padding: 10px;
  margin: 0 1rem;
  ${mq["md"]} {
    grid-template-columns: 1fr 1fr 1fr;
  }
  ${mq["lg"]} {
    grid-template-columns: 1fr 1fr 1fr 1fr;
  }
  ${mq["xl"]} {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
  }
`;

const link = css`
  text-decoration: none;
`;
const PokemonContainer = ({ pokemon, key }) => {
  const { pokemons, setPokemonImage } = useContext(PokemonContext);
  return (
    <div key={key} css={styles}>
      {pokemons !== undefined && pokemons.length > 0
        ? pokemons.map((pokemon) => {
            return (
              <Link to={`/pokemon/${pokemon.name}/${pokemon.id}`} css={link}>
                <Pokemon pokemon={pokemon} key={pokemon.id} />{" "}
              </Link>
            );
          })
        : null}
    </div>
  );
};

export default PokemonContainer;
