import React from "react";
import { BreadCrumb } from "../../components";
import PokemonContainer from "./PokemonContainer";

const Pokedex = () => {
  return (
    <>
      <BreadCrumb pageTitle="Pokedex" />
      <PokemonContainer />
    </>
  );
};

export default Pokedex;
