import React from "react";
import { Route, Switch } from "react-router-dom";
import { Navbar } from "./components";
import { MyPokemon, Pokedex, PokemonDetail } from "./views";
import "./index.css";
const App = () => {
  return (
    <div className="App">
      <Navbar />
      <Switch>
        <Route path="/" exact component={Pokedex} />
        <Route path="/pokemon/:name/:id" component={PokemonDetail} />
        <Route path="/mypokemon" component={MyPokemon} />
      </Switch>
    </div>
  );
};

export default App;
