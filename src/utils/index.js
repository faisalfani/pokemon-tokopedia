const breakpoints = { sm: 576, md: 768, lg: 992, xl: 1200 };

const mq = {};

Object.keys(breakpoints).forEach((bp) => {
  mq[bp] = `@media (min-width: ${breakpoints[bp]}px)`;
});

const getBackground = (types) => {
  let typeName = "";
  let bg = "";
  if (types !== undefined) {
    typeName = types.map((data) => data.type.name).join(",");
    if (typeName.includes("grass")) {
      bg = "#159F6E";
    } else if (typeName.includes("fire") || typeName.includes("fighting")) {
      bg = "#F15B51";
    } else if (typeName.includes("bug") || typeName.includes("fairy")) {
      bg = "#8F36C6";
    } else if (typeName.includes("water")) {
      bg = "#366FC6";
    } else if (typeName.includes("normal") || typeName.includes("flying")) {
      bg = "#B0B8C7";
    } else if (typeName.includes("ice")) {
      bg = "#6BC4EE";
    } else if (typeName.includes("electric")) {
      bg = "#F7A61B";
    } else if (
      typeName.includes("ground") ||
      typeName.includes("rock") ||
      typeName.includes("steel")
    ) {
      bg = "#AF641F";
    } else {
      bg = "#F5F6F7";
    }
  }

  return bg;
};

const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export { mq, getBackground, capitalizeFirstLetter };
