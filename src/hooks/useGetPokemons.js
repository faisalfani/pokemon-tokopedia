import React from "react";
import { useQuery } from "@apollo/client";
import { GET_POKEMONS } from "../graphql/queries";

const useGetPokemons = (limit, offset) => {
  const { data } = useQuery(GET_POKEMONS, {
    variables: {
      limit,
      offset,
    },
  });
  return {
    nextOffset: data?.pokemons.nextOffset,
    pokemons: data?.pokemons.results,
  };
};

export default useGetPokemons;
