import React from "react";
import { useQuery } from "@apollo/client";
import { GET_POKEMON } from "../graphql/queries";

const useGetPokemon = (name) => {
  const { data } = useQuery(GET_POKEMON, {
    variables: {
      name,
    },
  });
  return {
    pokemon: data?.pokemon,
  };
};

export default useGetPokemon;
