import useGetPokemons from "./useGetPokemons";
import useGetType from "./useGetType";

export { useGetPokemons, useGetType };
