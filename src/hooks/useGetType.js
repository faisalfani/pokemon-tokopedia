import React from "react";
import { useQuery } from "@apollo/client";
import { GET_TYPE } from "../graphql/queries";

const useGetType = (name) => {
  const { data } = useQuery(GET_TYPE, {
    variables: {
      name,
    },
  });
  return data?.pokemon?.types;
};

export default useGetType;
