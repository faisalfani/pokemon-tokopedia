export const pokemons = [];
export const pokemon = [];
export const myPokemon = [];

const defaultContext = {
  pokemons,
  pokemon,
  myPokemon,
};

export default defaultContext;
