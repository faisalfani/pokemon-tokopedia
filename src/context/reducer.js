import { constants } from "./constants";

const reducer = (state, action) => {
  switch (action.type) {
    case constants.GETALL_SUCCESS:
      return { ...state, count: state.count + 1 };
    case "decrement":
      return { ...state, count: state.count - 1 };

    default:
      return state;
  }
};

export default reducer;
