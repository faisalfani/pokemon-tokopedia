import React, { createContext, useEffect, useState } from "react";
import { useGetPokemons } from "../hooks";
import useGetPokemon from "../hooks/useGetPokemon";
export const PokemonContext = createContext();

const PokemonContextProvider = ({ children }) => {
  const [limit, setLimit] = useState(18);
  const [offset, setOffset] = useState(0);
  const [pokemonName, setPokemonName] = useState("");

  const { pokemons, nextOffset } = useGetPokemons(limit, offset);
  const { pokemon } = useGetPokemon(pokemonName);

  const nextPage = () => {
    setOffset(nextOffset);
  };

  return (
    <PokemonContext.Provider
      value={{
        pokemons,
        pokemon,
        setLimit,
        setOffset,
        nextPage,
        setPokemonName,
      }}
    >
      {children}
    </PokemonContext.Provider>
  );
};

export default PokemonContextProvider;
