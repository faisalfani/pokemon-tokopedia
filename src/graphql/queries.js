import { gql } from "@apollo/client";

export const GET_POKEMONS = gql`
  query getPokemons($limit: Int, $offset: Int) {
    pokemons(limit: $limit, offset: $offset) {
      prevOffset
      nextOffset
      results {
        id
        name
        image: dreamworld
      }

      status
      message
    }
  }
`;

export const GET_TYPE = gql`
  query getType($name: String!) {
    pokemon(name: $name) {
      types {
        type {
          name
        }
      }
    }
  }
`;

export const GET_POKEMON = gql`
  query getPokemon($name: String!) {
    pokemon(name: $name) {
      height
      weight
      stats {
        stat {
          name
        }
        base_stat
      }
      abilities {
        ability {
          name
        }
      }
    }
  }
`;
