import React from "react";
import { mq } from "./../../utils";
import { jsx, css } from "@emotion/react";
import { CgChevronLeft, CgPokemon } from "react-icons/cg";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";

const styles = {
  navContainer: css`
    border-radius: 10px;
    height: 48px;
  `,
  navItems: css`
    height: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 1rem 1.8rem;
    a {
      color: black;
      text-decoration: none;
      display: flex;
      align-items: center;
      column-gap: 8px;
      p {
        font-weight: 500;
        position: relative;
      }
      div {
        position: relative;
        span {
          position: absolute;
          font-size: x-small;
          font-weight: bold;
          color: white;
          background-color: red;
          padding: 2px 6px;
          border-radius: 100px;
          top: -3px;
          right: -7px;
        }
      }
    }
  `,
};

const Navbar = () => {
  const location = useLocation();
  return (
    <div css={styles.navContainer}>
      <div css={styles.navItems}>
        {location.pathname != "/" ? (
          <Link to="/">
            <CgChevronLeft size="24" />
          </Link>
        ) : (
          <div> </div>
        )}

        <Link to="/mypokemon">
          <p>My Pokemon</p>
          <div>
            <CgPokemon size="30" />
            <span>2</span>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default Navbar;
