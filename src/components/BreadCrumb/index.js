import React from "react";
import { jsx, css } from "@emotion/react";
import { capitalizeFirstLetter } from "../../utils";

const BreadCrumb = ({ pageTitle, color }) => {
  const styles = css`
    margin: 0 2rem;
    color: ${color !== undefined ? color : "black"};
  `;
  return <h1 css={styles}>{capitalizeFirstLetter(pageTitle)}</h1>;
};

export default BreadCrumb;
