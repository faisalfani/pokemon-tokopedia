import { css } from "@emotion/react";
import { Img } from "react-image";
import VisibilitySensor from "react-visibility-sensor";
import skeletonImage from "./../../assets/img/pokemonSkeleton.png";

const PokemonImage = ({ src, styles }) => (
  <VisibilitySensor>
    <Img
      src={src}
      css={styles}
      loader={<img css={styles} src={skeletonImage} alt="skeleton" />}
    />
  </VisibilitySensor>
);

export default PokemonImage;
