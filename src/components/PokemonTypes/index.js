import { css } from "@emotion/react";
import React from "react";

const PokemonTypes = ({ types, row }) => {
  const styles = {
    pokemonTypes: css`
      list-style: none;
      padding: 0;
      display: flex;
      flex-direction: ${row ? "row" : "column"};
      ${row ? "column-gap" : "row-gap"}: 8px;
    `,
    pokemonType: css`
      display: inline-block;
      width: fit-content;
      padding: 5px 12px;
      opacity: 0.8;
      background-color: rgba(255, 255, 255, 0.3);
      border-radius: 15px;
      font-size: xx-small;
      font-weight: 500;
      color: white;
    `,
  };
  return (
    <ul css={styles.pokemonTypes}>
      {types !== undefined && types.length > 0
        ? types.map((type) => (
            <li key={type.type.name} css={styles.pokemonType}>
              {type.type.name}
            </li>
          ))
        : null}
    </ul>
  );
};

export default PokemonTypes;
